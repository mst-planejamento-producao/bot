// Supports ES6
// import { create, Whatsapp } from 'venom-bot';
const venom = require('venom-bot');

venom
  .create({
    session: 'session-mst' //name of session
  })
  .then((client) => start(client))
  .catch((erro) => {
    console.log(erro);
  });

var nome = '';
var sobrenome = '';
var familia = '';
var menuOption = '';
var data = '';
var produto = '';
var quantidade = '';


function start(client) {

  var nomeReady = false;
  var sobrenomeReady = false;
  var menuReady = false;
  var familiaReady = false;
  var option = false;

  var produtoDefinido = false;
  var dataDefinida = false
  var quantidadeDefinida = false;
  var confirmacao = false;

  client.onMessage((message) => {
    if (message.body === 'Oi' && message.isGroupMsg === false) {
      client.sendText(message.from,'Olá, qual seu primeiro nome ? ');
      nomeReady = true;
    }
    else if(nomeReady){
      nome = message.body;
      client.sendText(message.from,'Legal! 😊 Prazer, '+nome+' ! E qual seu sobrenome? ');
      nomeReady = false;
      sobrenomeReady = true;
    }
    else if(sobrenomeReady){
      sobrenome = message.body;
      client.sendText(message.from,'E como devemos chamar a família a qual você faz parte?')
      sobrenomeReady = false;
      familiaReady = true;
    }
    else if(familiaReady){
      familia = message.body;
      client.sendText(message.from,`Ok, Fizemos o seu cadastro no sistema!!🥳🥳\n
Nome:`+nome+` `+sobrenome+`\n
Família:`+familia+`\n\n
O que você gostaria de fazer hoje? 
Digite o número da opção abaixo:\n 
*0 - Oferecer novos produtos*🥗\n 
*1-Ver os produtos que eu ofereci*📋\n 
*2-Alterar oferta cadastrada* ✏️\n 
*3-Enviar mensagem para coordenação📞*`);
      familiaReady = false;
      option = true;
    }
    else if(option){
      menuOption = message.body;
      if(menuOption != "0" && menuOption !="1" && menuOption != "2" && menuOption != "3")
        client.sendText(message.from,'Opção inválida, tente novamente 👍');
      else
        option = false;
    }
    if(menuOption === '0'){
      client.sendText(message.from,'Ok, qual produto você que você tem para oferecer?');
      menuOption = '';
      produtoDefinido = true;

    }
    else if(produtoDefinido){
      produto = message.body.toLowerCase(); 
      client.sendText(message.from,'E quando o produto '+produto+' estará disponível para a cesta?');
      produtoDefinido = false;
      dataDefinida = true;
    }
    else if(dataDefinida){
      dataDefinida = false;
      quantidadeDefinida = true;
      data = message.body;
      client.sendText(message.from,'Qual a quantidade estimada? (exemplo: 10 caixas, 5 kilos, etc)?');
    }
    else if(quantidadeDefinida){
      quantidadeDefinida = false;
      quantidade = message.body.toLowerCase();
      client.sendText(message.from,nome+', resumindo então, seria:\n'+quantidade+' de '+produto+' para '+data+'.\n*Certo??(Sim ou Não)*')
      confirmacao = true;
    }
    else if(confirmacao){
      var confirmacaoTxt = message.body.toLowerCase().replace("ã",'a');
      if(confirmacaoTxt === "sim"){
        confirmacao = false;
        client.sendText(message.from,'Produto cadastrado!!🫡😍');

      }
      else if (confirmacaoTxt === "nao"){
        menuOption = "0";
        confirmacao = false;
        confirmacaoTxt = "";
        client.sendText(message.from,'Caso queira cadastrar novamente então digite 0')

      }
      else{
        client.sendText(message.from,'Não entendi... Você confirma a sua oferta de '+produto+'?');
      }

    }
    if(menuOption === '1'){
      client.sendText(message.from,'Ver');
      menuOption = '';

    }
    else if(menuOption === '2'){
      client.sendText(message.from,'Opção em desenvolvimento, em breve estára disponível, ok??');
      menuOption = '';

    }
    else if(menuOption === '3'){
      client.sendText(message.from,'Clique abaixo para enviar uma mensagem:\n\nhttps://wa.me/552186635096');
      menuOption = '';

    
    }
  });
}